// ============ NAV ============

let mainNav = document.getElementById("mainNav");

let hamburgerMenu = document.getElementById("hamburgerMenu");

hamburgerMenu.addEventListener('click', ()=>{
    mainNav.classList.toggle('navVisible');

    let source = hamburgerMenu.getAttribute('src');
    
    if(source === "/img/burger-ferme.png"){
        hamburgerMenu.setAttribute('src', "/img/burger-ouvert.png");
    } else{
        hamburgerMenu.setAttribute('src', "/img/burger-ferme.png");
    }
    

});

// ============ CAROUSSEL TEMOINS ============

let right1 = document.getElementById('right1');
let right2 = document.getElementById('right2');

let left2 = document.getElementById('left2');
let left3 = document.getElementById('left3');

let container1 = document.getElementById('container1');
let container2 = document.getElementById('container2');
let container3 = document.getElementById('container3');


right1.addEventListener('click', ()=>{
    container2.classList.add('container2-right');
    container1.classList.add('container1-right');
});

left2.addEventListener('click', ()=>{
    container2.classList.remove('container2-right');
    container1.classList.remove('container1-right');

});

right2.addEventListener('click', ()=>{
    container3.classList.add('container3-right');
    container2.classList.add('container2-right-right');

});

left3.addEventListener('click', ()=>{
    container3.classList.remove('container3-right');
    container2.classList.remove('container2-right-right');

});

// ============ CAROUSSEL SOUTIENS ============

let container4 = document.getElementById("container4");

let container5 = document.getElementById("container5");

let container6 = document.getElementById("container6");

let stopButton = document.getElementById("stop");

stopButton.addEventListener('click', ()=>{
    container4.classList.toggle('pause');
    container5.classList.toggle('pause');
    container6.classList.toggle('pause');
    stopButton.classList.toggle('pause');

    let source2 = stopButton.getAttribute('src');
    
    if(source2 === "/img/icon-stop.png"){
        stopButton.setAttribute('src', "/img/icons8-bouton-lecture-entouré-30.png");
    } else{
        stopButton.setAttribute('src', "/img/icon-stop.png");
    }

});
